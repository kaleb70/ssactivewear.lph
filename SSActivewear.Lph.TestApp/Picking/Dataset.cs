﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.TestApp.Picking
{
    public static class Dataset
    {
        /// <summary>
        /// A convenience method for brevity.
        /// </summary>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static TimeSpan TmSpan(int hour, int minute) => new TimeSpan(hour, minute, 0);

        public static TestCase[] TestCases { get; } = new[]
        {
            new TestCase("picking.standardShift.01", new DateTime(2021, 6, 14), 61, 11.00m, TmSpan(8, 56), TmSpan(20, 26))
                    .AddPause(6224308, 38, TmSpan(12, 57), TmSpan(13, 39), TmSpan(13, 0), TmSpan(13, 30)) // lunch
                    .AddPause(6224308, 124, TmSpan(14, 58), TmSpan(15, 20)) // afternoon break
                .AddBatch(6221886, 17, TmSpan(9, 5), TmSpan(9, 21))
                .AddBatch(6222764, 38, TmSpan(9, 24), TmSpan(9, 49))
                .AddBatch(6223198, 140, TmSpan(9, 55), TmSpan(12, 16))
                .AddBatch(6224308, 164, TmSpan(12, 25), TmSpan(16, 0))
                .AddBatch(6226203, 156, TmSpan(16, 12), TmSpan(18, 28))
                .AddBatch(6227693, 24, TmSpan(18, 33), TmSpan(18, 55))
                .AddBatch(6228080, 35, TmSpan(18, 59), TmSpan(19, 18))
                .AddBatch(6228216, 27, TmSpan(19, 22), TmSpan(19, 42))
                .AddBatch(6228303, 23, TmSpan(19, 46), TmSpan(20, 5))
                .AddBatch(6225462, 5, TmSpan(20, 8), TmSpan(20, 15)),

            new TestCase("picking.standardShift.02", new DateTime(2021, 6, 15), 62, 9.82m, TmSpan(10, 53), TmSpan(21, 12))
                    .AddPause(6230902, 82, TmSpan(13, 32), TmSpan(14, 6), TmSpan(13, 33), TmSpan(14, 3)) // lunch
                    .AddPause(6231402, 8, TmSpan(15, 57), TmSpan(16, 19)) // afternoon break
                .AddBatch(6229219, 51, TmSpan(11, 4), TmSpan(11, 44))
                .AddBatch(6229946, 42, TmSpan(11, 48), TmSpan(12, 30))
                .AddBatch(6230902, 87, TmSpan(12, 33), TmSpan(14, 9))
                .AddBatch(6231406, 54, TmSpan(14, 15), TmSpan(14, 59))
                .AddBatch(6232286, 49, TmSpan(15, 6), TmSpan(15, 47))
                .AddBatch(6231402, 16, TmSpan(15, 52), TmSpan(16, 35))
                .AddBatch(6231542, 10, TmSpan(16, 41), TmSpan(16, 52))
                .AddBatch(6231982, 7, TmSpan(16, 57), TmSpan(17, 11))
                .AddBatch(6231545, 43, TmSpan(17, 16), TmSpan(17, 54))
                .AddBatch(6234015, 35, TmSpan(17, 59), TmSpan(18, 23))
                .AddBatch(6234154, 54, TmSpan(18, 25), TmSpan(18, 57))
                .AddBatch(6233994, 40, TmSpan(19, 1), TmSpan(19, 35))
                .AddBatch(6234443, 22, TmSpan(19, 40), TmSpan(20, 3))
                .AddBatch(6230406, 14, TmSpan(20, 6), TmSpan(20, 23))
                .AddBatch(6230303, 10, TmSpan(20, 25), TmSpan(20, 35))
                .AddBatch(6230770, 8, TmSpan(20, 37), TmSpan(20, 53))
                .AddBatch(6230333, 3, TmSpan(20, 56), TmSpan(21, 0)),

            new TestCase("picking.standardShift.03", new DateTime(2021, 6, 16), 61, 7.62m, TmSpan(10, 58), TmSpan(19, 5))
                    .AddPause(6237554, 42, TmSpan(13, 28), TmSpan(14, 5), TmSpan(13, 31), TmSpan(14, 1)) // lunch
                    .AddPause(6238983, 12, TmSpan(15, 57), TmSpan(16, 19)) // afternoon break
                .AddBatch(6236494, 14, TmSpan(11, 7), TmSpan(11, 18))
                .AddBatch(6236649, 47, TmSpan(11, 21), TmSpan(11, 59))
                .AddBatch(6236935, 66, TmSpan(12, 5), TmSpan(12, 56))
                .AddBatch(6237554, 51, TmSpan(13, 1), TmSpan(14, 15))
                .AddBatch(6238284, 13, TmSpan(14, 18), TmSpan(14, 32))
                .AddBatch(6238308, 46, TmSpan(14, 37), TmSpan(15, 17))
                .AddBatch(6238917, 22, TmSpan(15, 21), TmSpan(15, 42))
                .AddBatch(6238983, 23, TmSpan(15, 45), TmSpan(16, 36))
                .AddBatch(6239626, 65, TmSpan(16, 41), TmSpan(17, 34))
                .AddBatch(6239492, 54, TmSpan(17, 38), TmSpan(18, 17))
                .AddBatch(6239137, 13, TmSpan(18, 20), TmSpan(18, 31))
                .AddBatch(6238300, 8, TmSpan(18, 34), TmSpan(18, 39))
                .AddBatch(6237996, 5, TmSpan(18, 40), TmSpan(18, 44))
                .AddBatch(6238407, 4, TmSpan(18, 46), TmSpan(18, 53))
                .AddBatch(6239967, 1, TmSpan(18, 55), TmSpan(18, 58)),

            new TestCase("picking.standardShift.04", new DateTime(2021, 6, 17), 53, 7.53m, TmSpan(10, 52), TmSpan(18, 54))
                    .AddPause(6242331, 46, TmSpan(13, 30), TmSpan(14, 9), TmSpan(13, 34), TmSpan(14, 4)) // lunch
                    .AddPause(6244582, 1, TmSpan(15, 57), TmSpan(16, 23)) // afternoon break
                .AddBatch(6241366, 25, TmSpan(10, 59), TmSpan(11, 35))
                .AddBatch(6242577, 41, TmSpan(11, 40), TmSpan(12, 19))
                .AddBatch(6241535, 2, TmSpan(12, 26), TmSpan(12, 28))
                .AddBatch(6242094, 20, TmSpan(12, 31), TmSpan(12, 46))
                .AddBatch(6242331, 48, TmSpan(12, 51), TmSpan(14, 10))
                .AddBatch(6244068, 16, TmSpan(14, 15), TmSpan(14, 24))
                .AddBatch(6243064, 10, TmSpan(14, 27), TmSpan(14, 40))
                .AddBatch(6244022, 27, TmSpan(14, 46), TmSpan(15, 7))
                .AddBatch(6244269, 20, TmSpan(15, 11), TmSpan(15, 30))
                .AddBatch(6244484, 2, TmSpan(15, 38), TmSpan(15, 40))
                .AddBatch(6244228, 6, TmSpan(15, 42), TmSpan(15, 46))
                .AddBatch(6244333, 2, TmSpan(15, 50), TmSpan(15, 52))
                .AddBatch(6244582, 17, TmSpan(15, 55), TmSpan(16, 39))
                .AddBatch(6245397, 57, TmSpan(16, 44), TmSpan(17, 34))
                .AddBatch(6245644, 27, TmSpan(17, 40), TmSpan(18, 3))
                .AddBatch(6245478, 20, TmSpan(18, 9), TmSpan(18, 31))
                .AddBatch(6245785, 4, TmSpan(18, 36), TmSpan(18, 45)),

            new TestCase("picking.longShift.01", new DateTime(2021, 6, 28), 53, 10.9m, TmSpan(8, 55), TmSpan(20, 20))
                    .AddPause(6284475, 47, TmSpan(10, 56), TmSpan(11, 3)) // morning break
                    .AddPause(6284958, 60, 6285517, 1, TmSpan(12, 21), TmSpan(13, 9), TmSpan(12, 33), TmSpan(13, 4)) // lunch
                    .AddPause(6285517, 101, TmSpan(14, 58), TmSpan(15, 18)) // afternoon break
                .AddBatch(6283532, 71, TmSpan(9, 15), TmSpan(10, 1))
                .AddBatch(6284475, 57, TmSpan(10, 7), TmSpan(11, 12))
                .AddBatch(6284958, 60, TmSpan(11, 19), TmSpan(12, 21))
                .AddBatch(6285517, 143, TmSpan(12, 29), TmSpan(15, 49))
                .AddBatch(6288014, 123, TmSpan(16, 0), TmSpan(18, 15))
                .AddBatch(6288874, 1, TmSpan(18, 21), TmSpan(18, 21))
                .AddBatch(6289335, 10, TmSpan(18, 28), TmSpan(18, 37))
                .AddBatch(6289418, 19, TmSpan(18, 43), TmSpan(19, 4))
                .AddBatch(6289103, 24, TmSpan(19, 11), TmSpan(19, 28))
                .AddBatch(6288976, 9, TmSpan(19, 32), TmSpan(19, 41))
                .AddBatch(6287592, 6, TmSpan(19, 51), TmSpan(20, 10)),

            new TestCase("picking.longShift.02", new DateTime(2021, 6, 29), 51, 9.12m, TmSpan(8, 59), TmSpan(18, 36))
                    .AddPause(6291342, 6, TmSpan(10, 46), TmSpan(11, 05)) // morning break
                    .AddPause(6292146, 13, 6292300, 1, TmSpan(12, 55), TmSpan(13, 38), TmSpan(13, 4), TmSpan(13, 34)) // lunch
                    .AddPause(6293423, 6, TmSpan(14, 56), TmSpan(15, 21)) // afternoon break
                .AddBatch(6290669, 16, TmSpan(9, 17), TmSpan(9, 31))
                .AddBatch(6290941, 14, TmSpan(9, 36), TmSpan(9, 54))
                .AddBatch(6291020, 31, TmSpan(10, 0), TmSpan(10, 34))
                .AddBatch(6291342, 25, TmSpan(10, 42), TmSpan(11, 26))
                .AddBatch(6291623, 32, TmSpan(11, 34), TmSpan(12, 3))
                .AddBatch(6291872, 9, TmSpan(12, 11), TmSpan(12, 27))
                .AddBatch(6292146, 13, TmSpan(12, 33), TmSpan(12, 55))
                .AddBatch(6292300, 25, TmSpan(13, 1), TmSpan(14, 1))
                .AddBatch(6293020, 49, TmSpan(14, 8), TmSpan(14, 46))
                .AddBatch(6293423, 7, TmSpan(14, 50), TmSpan(15, 20))
                .AddBatch(6293775, 26, TmSpan(15, 31), TmSpan(16, 3))
                .AddBatch(6294205, 46, TmSpan(16, 10), TmSpan(16, 47))
                .AddBatch(6294413, 18, TmSpan(16, 50), TmSpan(17, 5))
                .AddBatch(6294692, 65, TmSpan(17, 12), TmSpan(17, 58))
                .AddBatch(6295072, 4, TmSpan(18, 4), TmSpan(18, 14))
                .AddBatch(6294649, 7, TmSpan(18, 17), TmSpan(18, 26)),

            new TestCase("picking.transfer", new DateTime(2021, 6, 16), 61, 6.67m, TmSpan(10, 58), TmSpan(19, 5))
                    .AddPause(6237554, 42, TmSpan(13, 28), TmSpan(14, 5), TmSpan(13, 31), TmSpan(14, 1)) // lunch
                    .AddPause(6238983, 12, TmSpan(15, 57), TmSpan(16, 19)) // afternoon break
                    .AddPause(6238983, 23, 6239492, 1, TmSpan(16, 36), TmSpan(17, 38), TmSpan(16, 39), TmSpan(17, 36)) // transfer
                .AddBatch(6236494, 14, TmSpan(11, 7), TmSpan(11, 18))
                .AddBatch(6236649, 47, TmSpan(11, 21), TmSpan(11, 59))
                .AddBatch(6236935, 66, TmSpan(12, 5), TmSpan(12, 56))
                .AddBatch(6237554, 51, TmSpan(13, 1), TmSpan(14, 15))
                .AddBatch(6238284, 13, TmSpan(14, 18), TmSpan(14, 32))
                .AddBatch(6238308, 46, TmSpan(14, 37), TmSpan(15, 17))
                .AddBatch(6238917, 22, TmSpan(15, 21), TmSpan(15, 42))
                .AddBatch(6238983, 23, TmSpan(15, 45), TmSpan(16, 36))
                //.AddBatch(6239626, 65, TmSpan(16, 41), TmSpan(17, 34)) // transfer
                .AddBatch(6239492, 54, TmSpan(17, 38), TmSpan(18, 17))
                .AddBatch(6239137, 13, TmSpan(18, 20), TmSpan(18, 31))
                .AddBatch(6238300, 8, TmSpan(18, 34), TmSpan(18, 39))
                .AddBatch(6237996, 5, TmSpan(18, 40), TmSpan(18, 44))
                .AddBatch(6238407, 4, TmSpan(18, 46), TmSpan(18, 53))
                .AddBatch(6239967, 1, TmSpan(18, 55), TmSpan(18, 58)),

            new TestCase("picking.transferSpansLunch", new DateTime(2021, 6, 17), 53, 6.15m, TmSpan(10, 52), TmSpan(18, 54))
                    .AddPause(6242094, 20, 6244068, 1, TmSpan(12, 46), TmSpan(14, 15), TmSpan(12, 49), TmSpan(14, 12)) // transfer
                    //.AddPause(6242331, 46, TmSpan(13, 30), TmSpan(14, 9), TmSpan(13, 34), TmSpan(14, 4)) // lunch
                    .AddPause(6244582, 1, TmSpan(15, 57), TmSpan(16, 23)) // afternoon break
                .AddBatch(6241366, 25, TmSpan(10, 59), TmSpan(11, 35))
                .AddBatch(6242577, 41, TmSpan(11, 40), TmSpan(12, 19))
                .AddBatch(6241535, 2, TmSpan(12, 26), TmSpan(12, 28))
                .AddBatch(6242094, 20, TmSpan(12, 31), TmSpan(12, 46))
                //.AddBatch(6242331, 48, TmSpan(12, 51), TmSpan(14, 10)) transfer
                .AddBatch(6244068, 16, TmSpan(14, 15), TmSpan(14, 24))
                .AddBatch(6243064, 10, TmSpan(14, 27), TmSpan(14, 40))
                .AddBatch(6244022, 27, TmSpan(14, 46), TmSpan(15, 7))
                .AddBatch(6244269, 20, TmSpan(15, 11), TmSpan(15, 30))
                .AddBatch(6244484, 2, TmSpan(15, 38), TmSpan(15, 40))
                .AddBatch(6244228, 6, TmSpan(15, 42), TmSpan(15, 46))
                .AddBatch(6244333, 2, TmSpan(15, 50), TmSpan(15, 52))
                .AddBatch(6244582, 17, TmSpan(15, 55), TmSpan(16, 39))
                .AddBatch(6245397, 57, TmSpan(16, 44), TmSpan(17, 34))
                .AddBatch(6245644, 27, TmSpan(17, 40), TmSpan(18, 3))
                .AddBatch(6245478, 20, TmSpan(18, 9), TmSpan(18, 31))
                .AddBatch(6245785, 4, TmSpan(18, 36), TmSpan(18, 45)),

            new TestCase("picking.transferSpansBreak", new DateTime(2021, 6, 15), 62, 9.02m, TmSpan(10, 53), TmSpan(21, 12))
                    .AddPause(6230902, 82, TmSpan(13, 32), TmSpan(14, 6), TmSpan(13, 33), TmSpan(14, 3)) // lunch
                    .AddPause(6232286, 49, 6231452, 1, TmSpan(15, 47), TmSpan(16, 41), TmSpan(15, 50), TmSpan(16, 38)) // transfer
                    //.AddPause(6231402, 8, TmSpan(15, 57), TmSpan(16, 19)) // afternoon break
                .AddBatch(6229219, 51, TmSpan(11, 4), TmSpan(11, 44))
                .AddBatch(6229946, 42, TmSpan(11, 48), TmSpan(12, 30))
                .AddBatch(6230902, 87, TmSpan(12, 33), TmSpan(14, 9))
                .AddBatch(6231406, 54, TmSpan(14, 15), TmSpan(14, 59))
                .AddBatch(6232286, 49, TmSpan(15, 6), TmSpan(15, 47))
                //.AddBatch(6231402, 16, TmSpan(15, 52), TmSpan(16, 35))
                .AddBatch(6231542, 10, TmSpan(16, 41), TmSpan(16, 52))
                .AddBatch(6231982, 7, TmSpan(16, 57), TmSpan(17, 11))
                .AddBatch(6231545, 43, TmSpan(17, 16), TmSpan(17, 54))
                .AddBatch(6234015, 35, TmSpan(17, 59), TmSpan(18, 23))
                .AddBatch(6234154, 54, TmSpan(18, 25), TmSpan(18, 57))
                .AddBatch(6233994, 40, TmSpan(19, 1), TmSpan(19, 35))
                .AddBatch(6234443, 22, TmSpan(19, 40), TmSpan(20, 3))
                .AddBatch(6230406, 14, TmSpan(20, 6), TmSpan(20, 23))
                .AddBatch(6230303, 10, TmSpan(20, 25), TmSpan(20, 35))
                .AddBatch(6230770, 8, TmSpan(20, 37), TmSpan(20, 53))
                .AddBatch(6230333, 3, TmSpan(20, 56), TmSpan(21, 0)),

            new TestCase("picking.longShift.03", new DateTime(2021, 7, 6), 60, 10.18m, TmSpan(8, 50), TmSpan(19, 31))
                    .AddPause(6291342, 6, TmSpan(9, 57), TmSpan(10, 15)) // morning break
                    .AddPause(6316203, 60, 6317342, 1, TmSpan(12, 27), TmSpan(13, 10), TmSpan(12, 34), TmSpan(13, 014)) // lunch
                    .AddPause(6318361, 4, 6318738, 1, TmSpan(14, 57), TmSpan(15, 25)) // afternoon break
                .AddBatch(6313116, 64, TmSpan(9, 1), TmSpan(9, 39))
                .AddBatch(6314350, 50, TmSpan(9, 45), TmSpan(10, 40))
                .AddBatch(6314320, 27, TmSpan(10, 45), TmSpan(11, 7))
                .AddBatch(6316203, 60, TmSpan(11, 15), TmSpan(12, 27))
                .AddBatch(6317342, 17, TmSpan(13, 10), TmSpan(13, 22))
                .AddBatch(6316731, 23, TmSpan(13, 27), TmSpan(13, 56))
                .AddBatch(6317951, 44, TmSpan(14, 4), TmSpan(14, 47))
                .AddBatch(6318361, 4, TmSpan(14, 51), TmSpan(14, 57))
                .AddBatch(6318738, 43, TmSpan(15, 25), TmSpan(16, 2))
                .AddBatch(6318935, 114, TmSpan(16, 10), TmSpan(18, 13))
                .AddBatch(6320068, 27, TmSpan(18, 18), TmSpan(18, 43))
                .AddBatch(6315926, 12, TmSpan(18, 49), TmSpan(19, 10))
                .AddBatch(6315392, 3, TmSpan(19, 14), TmSpan(19, 16))
                .AddBatch(6315387, 2, TmSpan(19, 19), TmSpan(19, 22)),
        };
    }
}
