﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSActivewear.Lph.Picking;

namespace SSActivewear.Lph.TestApp.Picking
{
    public class TestCase
    {
        public string Name { get; set; }

        public DateTime Date { get; set; }

        public int OldTabletLph { get; set; }

        public decimal PaylocityHours { get; set; }

        public List<Pause> Pauses { get; set; } = new List<Pause>();

        public List<Batch> Batches { get; set; } = new List<Batch>();

        public DateTime ClockIn { get; set; }

        public DateTime ClockOut { get; set; }

        public TestCase(string name, DateTime date, int oldTabletLph, decimal paylocityHours, TimeSpan clockIn, TimeSpan clockOut)
        {
            Name = name;
            Date = date.Date;
            OldTabletLph = oldTabletLph;
            PaylocityHours = paylocityHours;
            ClockIn = Date.Add(clockIn);
            ClockOut = Date.Add(clockOut);
        }

        public TestCase AddPause(long batchBefore, int lineBefore, long batchAfter, int lineAfter, TimeSpan lineBeforeCompleted, TimeSpan lineAfterCompleted, TimeSpan? clockOut, TimeSpan? clockIn)
        {
            Pauses.Add(new Pause
            {
                LineBefore = new SimulatedLine(batchBefore, Date.Add(lineBeforeCompleted), lineBefore),
                LineAfter = new SimulatedLine(batchAfter, Date.Add(lineAfterCompleted), lineAfter),
                ClockOut = clockOut.HasValue ? Date.Add(clockOut.Value) : default(DateTime?),
                ClockIn = clockIn.HasValue ? Date.Add(clockIn.Value) : default(DateTime?),
            });
            return this;
        }

        public TestCase AddPause(long batchBefore, int lineBefore, long batchAfter, int lineAfter, TimeSpan lineBeforeCompleted, TimeSpan lineAfterCompleted)
            => AddPause(batchBefore, lineBefore, batchAfter, lineAfter, lineBeforeCompleted, lineAfterCompleted, null, null);

        public TestCase AddPause(long batchBefore, int lineBefore, TimeSpan lineBeforeCompleted, TimeSpan lineAfterCompleted, TimeSpan? clockOut, TimeSpan? clockIn)
            => AddPause(batchBefore, lineBefore, batchBefore, lineBefore + 1, lineBeforeCompleted, lineAfterCompleted, clockOut, clockIn);

        public TestCase AddPause(long batchID, int lineBefore, TimeSpan lineBeforeCompleted, TimeSpan lineAfterCompleted)
            => AddPause(batchID, lineBefore, batchID, lineBefore + 1, lineBeforeCompleted, lineAfterCompleted, null, null);

        public TestCase AddBatch(long batchID, int lines, TimeSpan startTime, TimeSpan endTime)
        {
            Batches.Add(new Batch
            {
                BatchID = batchID,
                Lines = lines,
                StartTime = Date.Add(startTime),
                EndTime = Date.Add(endTime),
            });
            return this;
        }

        public SimulatedLine[] GetLines()
        {
            Pauses.Sort();

            var result = new List<SimulatedLine>();

            foreach (var batch in Batches)
                result.AddRange(GetLines(batch));

            return result.ToArray();
        }

        public SimulatedLine[] GetLines(Batch batch)
        {
            var result = new List<SimulatedLine>();

            result.Add(new SimulatedLine(batch.BatchID, batch.StartTime, 1));

            foreach (var pause in Pauses)
            {
                if (pause.LineBefore.BatchID.Equals(batch.BatchID))
                {
                    if (!result.Any(l => l.LineNumber == pause.LineBefore.LineNumber))
                        result.Add(pause.LineBefore);

                    if (pause.LineAfter.BatchID.Equals(batch.BatchID))
                    {
                        if (!result.Any(l => l.LineNumber == pause.LineAfter.LineNumber))
                            result.Add(pause.LineAfter);
                    }
                    else
                        break;
                }
                else if (pause.LineAfter.BatchID.Equals(batch.BatchID) && pause.LineAfter.LineNumber == 1)
                    result[0] = pause.LineAfter;
            }

            if (!result.Any(l => l.LineNumber == batch.Lines))
                result.Add(new SimulatedLine(batch.BatchID, batch.EndTime, batch.Lines));

            for (int index = result.Count - 1; index > 0; index--)
            {
                var rangeStartLine = result[index - 1].LineNumber;
                var linesNeeded = result[index].LineNumber - rangeStartLine - 1;
                if (linesNeeded > 0)
                {
                    var rangeStartTime = result[index - 1].TimeCompleted;
                    var rangeTimeSpanTicks = (result[index].TimeCompleted - rangeStartTime).Ticks;
                    for (int newLine = linesNeeded; newLine > 0; newLine--)
                    {
                        result.Insert(index, new SimulatedLine(batch.BatchID,
                                                                rangeStartTime.AddTicks(rangeTimeSpanTicks * newLine / (linesNeeded + 1)),
                                                                rangeStartLine + newLine));
                    }
                }
            }

            return result.ToArray();
        }

        public IEnumerable<SimulationStep> RunSimulation()
        {
            var lines = GetLines();

            var firstActivity = lines[0].TimeCompleted;
            var lastActivityOfDay = lines[lines.Length - 1].TimeCompleted;

            for (int completedLinesCount = 1; completedLinesCount <= lines.Length; completedLinesCount++)
            {
                var currentLine = lines[completedLinesCount - 1];
                var latestActivity = currentLine.TimeCompleted;

                var result = new SimulationStep(lines)
                {
                    FirstActivity = firstActivity,
                    LatestActivity = latestActivity,
                    CompletedLinesCount = completedLinesCount,
                };

                result.OldTabletLphTime = new TimeSpan(0, 1, 0);
                foreach (var batch in Batches)
                {
                    if (batch.EndTime <= latestActivity)
                        result.OldTabletLphTime += batch.EndTime - batch.StartTime;

                    else if (batch.StartTime < latestActivity)
                        result.OldTabletLphTime += latestActivity - batch.StartTime;
                }

                result.TimeclockLphTime = ClockOut - lastActivityOfDay; // start with just the end-of-day adjustment
                var lastClockIn = ClockIn;
                foreach (var pause in Pauses.Where(b => b.ClockOut < latestActivity).OrderBy(b => b.ClockOut))
                {
                    result.TimeclockLphTime += pause.ClockOut.Value - lastClockIn;
                    lastClockIn = pause.ClockIn.Value;
                }
                result.TimeclockLphTime += latestActivity - lastClockIn;

                yield return result;
            }
        }
    }
}
