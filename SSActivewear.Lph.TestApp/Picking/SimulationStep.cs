﻿using SSActivewear.Lph.Picking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.TestApp.Picking
{
    public class SimulationStep
    {
        private readonly SimulatedLine[] lines;

        public SimulationStep(SimulatedLine[] lines)
        {
            this.lines = lines;
        }

        public DateTime FirstActivity { get; set; }

        public DateTime LatestActivity { get; set; }

        public int CompletedLinesCount { get; set; }

        public SimulatedLine[] GetCompletedLines() => lines.Take(CompletedLinesCount).ToArray();

        public TimeSpan OldTabletLphTime { get; set; }

        public TimeSpan TimeclockLphTime { get; set; }
    }
}
