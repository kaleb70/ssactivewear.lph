﻿using SSActivewear.Lph.Picking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.TestApp.Picking
{
    public class Pause : IComparable<Pause>
    {
        public SimulatedLine LineBefore { get; set; }
        public SimulatedLine LineAfter { get; set; }

        public DateTime? ClockOut { get; set; }
        public DateTime? ClockIn { get; set; }

        public int CompareTo(Pause other) => LineBefore.TimeCompleted.CompareTo(LineAfter.TimeCompleted);
    }
}
