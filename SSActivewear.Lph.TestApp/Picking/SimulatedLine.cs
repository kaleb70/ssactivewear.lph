﻿using SSActivewear.Lph.Picking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.TestApp.Picking
{
    public class SimulatedLine : PickingLine
    {
        public int LineNumber { get; set; }

        public SimulatedLine(long batchID, DateTime timeCompleted, int lineNumber)
            : base(batchID, timeCompleted)
        {
            LineNumber = lineNumber;
        }
    }
}
