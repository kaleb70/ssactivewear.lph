﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SSActivewear.Lph.Picking;

namespace SSActivewear.Lph.TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var folderName = $"results-{DateTime.Now:yyyy.MM.dd-HH.mm.ss.fff}";
            Directory.CreateDirectory(folderName);

            // TODO: create the LphTimeEstimator for picking
            var lphEstimator = LphTimeEstimator.Create<IPickingActivityProvider, PickingActivityEvaluator>();

            using (var summaryWriter = new StreamWriter($@"{folderName}\summary.csv"))
            {
                summaryWriter.WriteLine("\"TestCaseName\",\"PaylocityHours\",\"OldTabletHours\",\"NewEstimatedHours\",\"PaylocityLph\",\"OldTabletLph\",\"NewEstimatedLph\",\"OldTabletAvgDeviation\",\"NewEstimateAvgDeviation\"");

                foreach (var testCase in Picking.Dataset.TestCases)
                {
                    TimeSpan oldTabletLphTime = new TimeSpan(0);
                    TimeSpan newEstimatedLphTime = new TimeSpan(0);
                    var oldTabletDeviation = new List<int>();
                    var newEstimateDeviation = new List<int>();

                    using (var detailWriter = new StreamWriter($@"{folderName}\detail.{testCase.Name}.csv"))
                    {
                        detailWriter.WriteLine("\"ActivityTime\",\"TimeclockLph\",\"OldTabletLph\",\"NewEstimatedLph\"");
                        foreach (var step in testCase.RunSimulation())
                        {
                            // TODO: create an activity provider
                            var activityProvider = new PickingActivityProvider(
                                                        "",
                                                        // put a custom or data-driven IActivityConfiguration here,
                                                        // or omit the argument to use the default
                                                        //customConfiguration,
                                                        step.FirstActivity,
                                                        step.LatestActivity,
                                                        step.CompletedLinesCount,
                                                        step.GetCompletedLines); // you provide a callback here that could do an expensive query as needed

                            // TODO: call GetLphTime()
                            newEstimatedLphTime = lphEstimator.GetLphTime(activityProvider);

                            // TODO: calculate the LPH
                            var newEstimatedLph = (int)(step.CompletedLinesCount / newEstimatedLphTime.TotalHours + .5);

                            var timeclockLph = (int)(step.CompletedLinesCount / step.TimeclockLphTime.TotalHours + .5);
                            var oldTabletLph = (int)(step.CompletedLinesCount / step.OldTabletLphTime.TotalHours + .5);

                            detailWriter.WriteLine($"{step.LatestActivity:s},{timeclockLph},{oldTabletLph},{newEstimatedLph}");

                            oldTabletLphTime = step.OldTabletLphTime;

                            oldTabletDeviation.Add(oldTabletLph - timeclockLph);
                            newEstimateDeviation.Add(newEstimatedLph - timeclockLph);
                        }
                        detailWriter.Close();
                    }

                    var completedLinesCount = testCase.Batches.Sum(b => b.Lines);

                    summaryWriter.Write($"\"{testCase.Name}\"");
                    summaryWriter.Write($",{testCase.PaylocityHours:0.00}");
                    summaryWriter.Write($",{oldTabletLphTime.TotalHours:0.00}");
                    summaryWriter.Write($",{newEstimatedLphTime.TotalHours:0.00}");
                    summaryWriter.Write($",{(int)(completedLinesCount / testCase.PaylocityHours + .5m)}");
                    summaryWriter.Write($",{(int)(completedLinesCount / oldTabletLphTime.TotalHours + .5)}");
                    summaryWriter.Write($",{(int)(completedLinesCount / newEstimatedLphTime.TotalHours + .5)}");
                    summaryWriter.Write($",{oldTabletDeviation.Average():0.0}");
                    summaryWriter.Write($",{newEstimateDeviation.Average():0.0}");
                    summaryWriter.WriteLine();
                }
                summaryWriter.Close();
            }
        }
    }
}
