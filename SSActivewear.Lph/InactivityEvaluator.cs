﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    public class InactivityEvaluator : IInactivityEvaluator
    {
        public static InactivityEvaluator Create() => new InactivityEvaluator();

        internal InactivityEvaluator()
        {
        }

        private TimeSpan GetAlternativeStartOfDay(DateTime firstActivity) => new TimeSpan(0, (firstActivity.Minute + 5) % 30, 0);

        public TimeSpan GetOverhead(IActivityProvider activityProvider)
        {
            var config = activityProvider.Configuration;
            return (config.AssumeStartOnHalfHour
                        ? Comparable.Max(GetAlternativeStartOfDay(activityProvider.FirstActivity),
                                            config.StartOfDayOverhead)
                        : config.StartOfDayOverhead)
                    + config.EndOfDayOverhead;
        }

        public TimeSpan EvaluateInactivity(IActivityProvider activityProvider, ActivityResult activityResult)
        {
            TimeSpan result = new TimeSpan(0);

            var config = activityProvider.Configuration;

            // a number used to detect breaks that might be mistaken for transfers
            var maxBreakTurnaround = activityResult.AverageTurnaround?.MultiplyBy(3)
                                        ?? Comparable.Min(config.MaximumTurnaround.MultiplyBy(2),
                                                            config.StartOfDayOverhead + config.EndOfDayOverhead);

            ActivityResult.InactivePeriod breakCandidate = null;
            bool gotBreak = false;

            var turnaround = activityResult.AverageTurnaround ?? config.MaximumTurnaround;

            ActivityResult.InactivePeriod lunch = null;

            foreach (var pause in activityResult.InactivePeriods)
            {
                if (pause.Length > config.StandardLunch && (lunch == null || pause.Length < lunch.Length))
                    lunch = pause;
            }

            // Here we fudge the turnaround because on-the-clock time tends to get wasted around lunch
            if (lunch != null)
                result += Comparable.Min(lunch.Length - config.StandardLunch, turnaround.MultiplyBy(2));

            foreach (var pause in activityResult.InactivePeriods)
            {
                if (pause != lunch)
                {
                    if (pause.IsPotentialTransfer && pause.Length >= config.MinimumTransfer)
                    {
                        result += turnaround;

                        // an apparent transfer after lunch might really be
                        // a break that happened to come between batches
                        if (!gotBreak
                                && pause.Length >= config.StandardBreak
                                && pause.Length <= config.StandardBreak + maxBreakTurnaround
                                && (config.IsBreakBeforeLunch
                                    ? lunch == null || pause.EndTime < lunch.StartTime
                                    : pause.StartTime > lunch?.EndTime)
                                && (breakCandidate == null || pause.Length < breakCandidate.Length))
                        {
                            breakCandidate = pause;
                        }
                    }

                    else
                    {
                        gotBreak |= pause.Length >= config.StandardBreak
                                    && (config.IsBreakBeforeLunch
                                        ? lunch == null || pause.EndTime < lunch.StartTime
                                        : pause.StartTime > lunch?.EndTime);
                        result += pause.Length;
                    }
                }
            }

            // if an apparent transfer was really a break, add the excluded time back in
            if (!gotBreak && breakCandidate != null)
                result += breakCandidate.Length - turnaround;

            return result;
        }
    }
}
