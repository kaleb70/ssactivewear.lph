﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    public static class TimeSpanExtensions
    {
        public static TimeSpan MultiplyBy(this TimeSpan span, double value) => new TimeSpan((long)(span.Ticks * value));

        public static TimeSpan DivideBy(this TimeSpan span, double value) => new TimeSpan((long)(span.Ticks / value));

        public static TimeSpan MultiplyBy(this TimeSpan span, long value) => new TimeSpan(span.Ticks * value);

        public static TimeSpan DivideBy(this TimeSpan span, long value) => new TimeSpan(span.Ticks / value);

        public static TimeSpan AddMinutes(this TimeSpan span, int minutes) => span + new TimeSpan(0, minutes, 0);
    }
}
