﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    /// <summary>
    /// An interface to provide numbers used to estimate time counted toward LPH.
    /// These are likely to vary by department, and possibly by warehouse.
    /// </summary>
    public interface IActivityConfiguration
    {
        /// <summary>
        /// Overhead at the start of the day.
        /// </summary>
        /// <remarks>
        /// This is different from the standard clockin time activated by
        /// <see cref="AssumeStartOnHalfHour"/>. Start-of-day overhead is
        /// the time a worker would typically take between clockin and
        /// completion of the first line if there were no intervening
        /// factors such as a morning meeting.
        /// </remarks>
        TimeSpan StartOfDayOverhead { get; }

        /// <summary>
        /// Overhead at the end of the day.
        /// </summary>
        /// <remarks>
        /// End-of-day overhead is the time a worker would typically take
        /// between completion of the last line of the day, and clockout.
        /// </remarks>
        TimeSpan EndOfDayOverhead { get; }

        /// <summary>
        /// Indicates that a standard clockin time of 25 or 55 past the
        /// hour should be used.
        /// </summary>
        /// <remarks>
        /// This overrides <see cref="StartOfDayOverhead"/> to properly account for
        /// morning meetings and other intervening factors at the start of the day.
        /// <see cref="StartOfDayOverhead"/> is still used in other circumstances, such
        /// as when the worker has taken a long lunch that problably involved leaving
        /// the premises.
        /// </remarks>
        bool AssumeStartOnHalfHour { get; }

        /// <summary>
        /// Indicates the length of the primary break of the day.
        /// </summary>
        TimeSpan StandardBreak { get; }

        /// <summary>
        /// Indicates the standard length of the lunch break.
        /// </summary>
        TimeSpan StandardLunch { get; }

        /// <summary>
        /// Indicates that the primary break of the day occurs before lunch instead of after.
        /// </summary>
        bool IsBreakBeforeLunch { get; }

        /// <summary>
        /// Indicates the minimum length of time that should be considered a possible
        /// transfer to another department. <see cref="OverheadEvaluator"/> uses this number
        /// in conjunction with <see cref="ActivityResult.InactivePeriod.IsPotentialTransfer"/>
        /// when deciding whether to exclude a period of inactivity from the total time
        /// counted toward LPH.
        /// </summary>
        TimeSpan MinimumTransfer { get; }

        /// <summary>
        /// Identifies the maximum turnaround time to be considered when computing averages.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Turnaround is the amount of time taken between bulk units of work, such as
        /// the time a picker takes between the end of one batch and the start of
        /// the next.
        /// </para>
        /// <para>
        /// <see cref="InactivityEvaluator"/> uses the average turnaround time when estimating
        /// the length of a transfer to another department. Turnarounds exceeding this value are
        /// excluded from the average calculation, so that anomalous turnarounds will not
        /// skew the results.
        /// </para>
        /// </remarks>
        TimeSpan MaximumTurnaround { get; }
    }
}
