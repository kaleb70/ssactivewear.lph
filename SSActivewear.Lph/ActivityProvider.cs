﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    public abstract class ActivityProvider : IActivityProvider
    {
        public string Key { get; private set; }

        public IActivityConfiguration Configuration { get; private set; }

        public DateTime FirstActivity { get; private set; }

        public DateTime LatestActivity { get; private set; }

        public int CompletedLinesCount { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"><para>A unique key for <see cref="IActivityProvider.Key"/>.</para>
        ///     <para><see cref="ActivityProvider"/> automatically includes <see cref="FirstActivity"/> in the property value.</para>
        /// </param>
        /// <param name="configuration"></param>
        /// <param name="firstActivity"></param>
        /// <param name="latestActivity"></param>
        /// <param name="completedLinesCount"></param>
        public ActivityProvider(string key, IActivityConfiguration configuration, DateTime firstActivity, DateTime latestActivity, int completedLinesCount)
        {
            Configuration = configuration;
            FirstActivity = firstActivity;
            LatestActivity = latestActivity;
            CompletedLinesCount = completedLinesCount;

            // The key does *not* contain LastActivity or CompletedLinesCount,
            // because the cache is for any recent value of LatestActivity.
            Key = $"FirstActivity={FirstActivity};{key}";
        }
    }
}
