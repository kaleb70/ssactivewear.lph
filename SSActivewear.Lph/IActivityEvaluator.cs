﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    /// <summary>
    /// An interface to provide a department-specific estimation of the time a worker was actively completing lines.
    /// </summary>
    /// <typeparam name="TActivityProvider">The subtype of <see cref="IActivityProvider"/> expected by the implementation.</typeparam>
    public interface IActivityEvaluator<TActivityProvider> where TActivityProvider : IActivityProvider
    {
        /// <summary>
        /// Estimates the portion of the activity reported by the specified activity provider,
        /// during which the worker was actively completing lines.
        /// </summary>
        /// <param name="provider"></param>
        /// <returns>An <see cref="ActivityResult"/> containing the total active time and a collection of inactive time periods to be evaluated by an <see cref="IInactivityEvaluator"/>.</returns>
        ActivityResult EvaluateActivity(TActivityProvider provider);
    }
}
