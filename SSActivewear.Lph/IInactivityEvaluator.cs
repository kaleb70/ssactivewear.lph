﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    /// <summary>
    /// An interface to estimate the portion of inactive time counted toward the LPH.
    /// </summary>
    public interface IInactivityEvaluator
    {
        /// <summary>
        /// Estimates the overhead for the activity reported by the specified <see cref="IActivityProvider"/>.
        /// </summary>
        /// <param name="activityProvider"></param>
        /// <returns></returns>
        /// <remarks>
        /// This is time that falls completely outside the reported activity, such as periods of inactivity
        /// at the start and end of the work day.
        /// </remarks>
        TimeSpan GetOverhead(IActivityProvider activityProvider);

        /// <summary>
        /// Estimates the portion of inactive time in the specified <see cref="ActivityResult"/>
        /// that is counted toward the LPH.
        /// </summary>
        /// <param name="activityProvider"></param>
        /// <param name="activityResult"></param>
        /// <returns></returns>
        TimeSpan EvaluateInactivity(IActivityProvider activityProvider, ActivityResult activityResult);
    }
}
