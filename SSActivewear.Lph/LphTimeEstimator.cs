﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    /// <summary>
    /// A non-generic base class for <see cref="LphTimeEstimator{TActivityProvider, TActivityEvaluator}"/>,
    /// to provide a factory method and simplify outside references to static members.
    /// </summary>
    public class LphTimeEstimator
    {
        public static LphTimeEstimator<TActivityProvider, TActivityEvaluator> Create<TActivityProvider, TActivityEvaluator>()
                where TActivityProvider : IActivityProvider
                where TActivityEvaluator : IActivityEvaluator<TActivityProvider>, new()
        {
            return new LphTimeEstimator<TActivityProvider, TActivityEvaluator>(
                        new TActivityEvaluator(), InactivityEvaluator.Create(), MemoryCache.Default);
        }

        protected LphTimeEstimator()
        {
        }

        internal class CacheEntry : IEquatable<CacheEntry>
        {
            public DateTime LatestActivity { get; set; }

            public TimeSpan LphTime { get; set; }

            public bool Equals(CacheEntry other)
            {
                return other != null
                        && LatestActivity == other.LatestActivity
                        && LphTime == other.LphTime;
            }

            public override bool Equals(object obj)
            {
                return obj?.GetType() == typeof(CacheEntry)
                        && Equals((CacheEntry)obj);
            }

            public override int GetHashCode()
            {
                return LatestActivity.GetHashCode() ^ LphTime.GetHashCode();
            }
        }
    }

    /// <summary>
    /// A class to estimate the time in a work day to be counted toward LPH.
    /// </summary>
    /// <typeparam name="TActivityProvider"></typeparam>
    /// <typeparam name="TActivityEvaluator"></typeparam>
    /// <see cref="LphTimeEstimator.Create{TActivityProvider, TActivityEvaluator}(TActivityEvaluator)"/>
    public class LphTimeEstimator<TActivityProvider, TActivityEvaluator> : LphTimeEstimator
            where TActivityProvider : IActivityProvider
            where TActivityEvaluator : IActivityEvaluator<TActivityProvider>
    {
        private readonly TActivityEvaluator activityEvaluator;
        private readonly IInactivityEvaluator inactivityEvaluator;
        private readonly ObjectCache cache;

        internal LphTimeEstimator(TActivityEvaluator activityEvaluator,
                                    IInactivityEvaluator inactivityEvaluator,
                                    ObjectCache cache)
        {
            this.activityEvaluator = activityEvaluator;
            this.inactivityEvaluator = inactivityEvaluator;
            this.cache = cache;
        }

        private CacheEntry GetCacheEntry(IActivityProvider activityProvider, Func<TimeSpan> getLphTime)
        {
            var key = "SSActivewear.Lph;" + activityProvider.Key;
            TimeSpan expiration = Comparable.Min(activityProvider.Configuration.MinimumTransfer,
                                                    activityProvider.Configuration.StandardLunch);
            var result = (CacheEntry)cache.Get(key);
            if (result == null
                    || activityProvider.LatestActivity - result.LatestActivity > expiration
                    || result.LatestActivity > activityProvider.LatestActivity) // <-- it's actually bad if this ever happens
            {
                result = new CacheEntry { LatestActivity = activityProvider.LatestActivity, LphTime = getLphTime() };

                var policy = new CacheItemPolicy();
                policy.AbsoluteExpiration = DateTimeOffset.Now.Add(expiration);

                cache.Set(key, result, policy);
            }
            return result;
        }

        /// <summary>
        /// Estimates the total time to be counted toward LPH for the
        /// activity reported by the specified activity provider.
        /// </summary>
        /// <param name="activityProvider"></param>
        /// <returns></returns>
        public TimeSpan GetLphTime(TActivityProvider activityProvider)
        {
            var cacheEntry = GetCacheEntry(activityProvider, () =>
            {
                var result = inactivityEvaluator.GetOverhead(activityProvider);

                var activityResult = activityEvaluator.EvaluateActivity(activityProvider);
                result += activityResult.TotalActiveTime;

                result += inactivityEvaluator.EvaluateInactivity(activityProvider, activityResult);

                return result;
            });

            return cacheEntry.LphTime + (activityProvider.LatestActivity - cacheEntry.LatestActivity);
        }
    }
}
