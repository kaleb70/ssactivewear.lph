﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.Picking
{
    /// <summary>
    /// An interface to report the activity of a picker.
    /// </summary>
    public interface IPickingActivityProvider : IActivityProvider
    {
        /// <summary>
        /// Returns a collection of the lines completed so far.
        /// The number of elements returned always equals
        /// <see cref="IActivityProvider.CompletedLinesCount"/>.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// <para>
        /// For the first line, <see cref="PickingLine.TimeCompleted"/>
        /// equals <see cref="IActivityProvider.FirstActivity"/>.
        /// </para>
        /// <para>
        /// For the last line, <see cref="PickingLine.TimeCompleted"/>
        /// equals <see cref="IActivityProvider.LatestActivity"/>.
        /// </para>
        /// <para>
        /// This is only called if there is no cached result for
        /// extrapolation, or if it has expired. Therefore, this
        /// method may perform expensive operations such as
        /// database queries.
        /// </para>
        /// </remarks>
        PickingLine[] GetCompletedLines();
    }
}
