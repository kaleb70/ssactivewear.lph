﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.Picking
{
    public class PickingActivityEvaluator : IActivityEvaluator<IPickingActivityProvider>
    {
        public ActivityResult EvaluateActivity(IPickingActivityProvider provider)
        {
            var result = new ActivityResult();
            var inactivePeriods = new List<ActivityResult.InactivePeriod>();

            var lines = provider.GetCompletedLines();

            // A pause shorter than this is not reported as an inactive period,
            // since it will count toward the LPH in all circumstances.
            var minInactive = Comparable.Min(provider.Configuration.StandardBreak, provider.Configuration.MinimumTransfer);

            // Tallies for computing average turnaround
            long turnaroundTotal = 0;
            long turnaroundCount = 0;

            var firstLine = lines[0];
            var prevLine = firstLine; // While looping, the line preceding the current one

            // While looping through the completed lines, this is the first
            // line after the last detected inactive period, or it is the first
            // line of the day.
            var firstLineSinceInactive = firstLine;

            foreach (var currentLine in lines.Skip(1))
            {
                try
                {
                    var timeBetweenLines = currentLine.TimeCompleted - prevLine.TimeCompleted;

                    // check for a new batch, and include in the average turnaround
                    if (!currentLine.BatchID.Equals(prevLine.BatchID) && timeBetweenLines <= provider.Configuration.MaximumTurnaround)
                    {
                        turnaroundTotal += timeBetweenLines.Ticks;
                        ++turnaroundCount;
                    }

                    // check for inactive time
                    if (timeBetweenLines >= minInactive)
                    {
                        // add to list
                        inactivePeriods.Add(new ActivityResult.InactivePeriod
                        {
                            StartTime = prevLine.TimeCompleted,
                            EndTime = currentLine.TimeCompleted,
                            IsPotentialTransfer = currentLine.BatchID != prevLine.BatchID,
                        });

                        // add this block of productive time to the result
                        result.TotalActiveTime += prevLine.TimeCompleted - firstLineSinceInactive.TimeCompleted;

                        // reset the block of productive time
                        firstLineSinceInactive = currentLine;
                    }
                }
                finally
                {
                    prevLine = currentLine;
                }
            }

            // add the final block of productive time to the result
            result.TotalActiveTime += prevLine.TimeCompleted - firstLineSinceInactive.TimeCompleted;

            // compute the average turnaround time, or use
            // the configured max if we don't have the data yet
            if (turnaroundCount != 0)
                result.AverageTurnaround = new TimeSpan(turnaroundTotal / turnaroundCount);

            result.InactivePeriods = inactivePeriods.ToArray();
            return result;
        }
    }
}
