﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.Picking
{
    public class PickingLine
    {
        public long BatchID { get; set; }

        public DateTime TimeCompleted { get; set; }

        public PickingLine(long batchID, DateTime timeCompleted)
        {
            BatchID = batchID;
            TimeCompleted = timeCompleted;
        }

        public override int GetHashCode()
        {
            return BatchID.GetHashCode() ^ TimeCompleted.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj != null && Equals((PickingLine)obj);
        }
        public bool Equals(PickingLine other)
        {
            return BatchID == other.BatchID && TimeCompleted == other.TimeCompleted;
        }

        /// <summary>
        /// For sorting purposes, <see cref="BatchID"/> is not considered.
        /// Use <see cref="Equals(CompletedLine{TBatchID})"/> to test for true equality.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(PickingLine other)
        {
            return TimeCompleted.CompareTo(other.TimeCompleted);
        }

        public override string ToString() => $"BatchID={BatchID}, TimeCompleted={TimeCompleted}";
    }
}
