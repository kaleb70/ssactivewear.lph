﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.Picking
{
    public class PickingActivityProvider : ActivityProvider, IPickingActivityProvider
    {
        private readonly Func<PickingLine[]> _getCompletedLines;

        private static ActivityConfiguration defaultConfiguration = new ActivityConfiguration()
        {
            StartOfDayOverhead = new TimeSpan(0, 10, 0),
            EndOfDayOverhead = new TimeSpan(0, 10, 0),
            AssumeStartOnHalfHour = true,
            StandardBreak = new TimeSpan(0, 15, 0),
            StandardLunch = new TimeSpan(0, 30, 0),
            IsBreakBeforeLunch = false,
            MinimumTransfer = new TimeSpan(0, 20, 0),
            MaximumTurnaround = new TimeSpan(0, 10, 0)
        };

        /// <summary>
        /// Creates a <see cref="PickingActivityProvider"/> using the default configuration for picking.
        /// </summary>
        /// <param name="key"><para>A unique key for <see cref="IActivityProvider.Key"/>.</para>
        ///     <para><see cref="PickingActivityProvider"/> automatically includes <see cref="ActivityProvider.FirstActivity"/> and the provider type in the property value.</para>
        /// </param>
        /// <param name="firstActivity">The time of the first line completion activity.</param>
        /// <param name="latestActivity">The time of the most recent line completion activity.</param>
        /// <param name="completedLinesCount">The number of lines completed so far.</param>
        /// <param name="getCompletedLines">A delegate implementing <see cref="IPickingActivityProvider.GetCompletedLines"/>.</param>
        /// <remarks>
        /// It is recommended that the provded key include a warehouse identifier, the department, and the employee ID.
        /// </remarks>
        public PickingActivityProvider(string key, DateTime firstActivity, DateTime latestActivity, int completedLinesCount, Func<PickingLine[]> getCompletedLines)
            : this(key, defaultConfiguration, firstActivity, latestActivity, completedLinesCount, getCompletedLines)
        {
        }

        /// <summary>
        /// Creates a <see cref="PickingActivityProvider"/> using a custom or data-driven configuration.
        /// </summary>
        /// <param name="key"><para>A unique key for <see cref="IActivityProvider.Key"/>.</para>
        ///     <para><see cref="PickingActivityProvider"/> automatically includes <see cref="ActivityProvider.FirstActivity"/> and the provider type in the property value.</para>
        /// </param>
        /// <param name="configuration">A custom or data-driven configuration.</param>
        /// <param name="firstActivity">The time of the first line completion activity.</param>
        /// <param name="latestActivity">The time of the most recent line completion activity.</param>
        /// <param name="completedLinesCount">The number of lines completed so far.</param>
        /// <param name="getCompletedLines">A delegate implementing <see cref="IPickingActivityProvider.GetCompletedLines"/>.</param>
        /// <remarks>
        /// It is recommended that the provded key include a warehouse identifier, the department, and the employee ID.
        /// </remarks>
        public PickingActivityProvider(string key, IActivityConfiguration configuration, DateTime firstActivity, DateTime latestActivity, int completedLinesCount, Func<PickingLine[]> getCompletedLines)
            : base($"Provider=PickingActivityProvider;{key}", configuration, firstActivity, latestActivity, completedLinesCount)
        {
            _getCompletedLines = getCompletedLines;
        }

        public PickingLine[] GetCompletedLines() => _getCompletedLines();
    }
}
