﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    /// <summary>
    /// Describes the result of a call to <see cref="IActivityEvaluator{TActivityProvider}.EvaluateActivity(TActivityProvider)"/>.
    /// </summary>
    public class ActivityResult
    {
        /// <summary>
        /// Total amount of time that the worker was actively completing lines.
        /// </summary>
        public TimeSpan TotalActiveTime { get; set; } = new TimeSpan(0);

        /// <summary>
        /// Average turnaround time between bulk work units, such as the time a picker takes between batches.
        /// </summary>
        public TimeSpan? AverageTurnaround { get; set; }

        /// <summary>
        /// A collection of inactive periods not included in <see cref="TotalActiveTime"/>.
        /// </summary>
        public InactivePeriod[] InactivePeriods { get; set; }

        /// <summary>
        /// Describes a pause in the worker's active completion of lines.
        /// </summary>
        public class InactivePeriod
        {
            /// <summary>
            /// The start time of the pause.
            /// </summary>
            public DateTime StartTime { get; set; }

            /// <summary>
            /// The end time of the pause.
            /// </summary>
            public DateTime EndTime { get; set; }

            /// <summary>
            /// The length of the pause.
            /// </summary>
            public TimeSpan Length => EndTime - StartTime;

            /// <summary>
            /// This pause might have been a transfer to another department.
            /// For example, a picker was between batches.
            /// </summary>
            public bool IsPotentialTransfer { get; set; }
        }
    }
}
