﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    /// <summary>
    /// An interface to report the activity of a worker.
    /// This should be the activity for a specific warehouse, department, and day,
    /// but the specifics are left up to the implementation.
    /// </summary>
    /// <remarks>
    /// This interface is used in the extrapolation of LPH time results from a previous
    /// complete calculation. Therefore, the backing properties should be preloaded with
    /// as little overhead as possible. However, the public API of a subtype is typically
    /// used when no cached calculation is available, and may perform more expensive
    /// operations such as database queries.
    /// </remarks>
    public interface IActivityProvider
    {
        /// <summary>
        /// A cache key for this activity provider.
        /// </summary>
        /// <remarks>
        /// The estimated hours may be cached, and subsequent estimations extrapolated from
        /// the cached result. This key uniquely identifies the context in which the calculation
        /// is valid. For example, it might identify the warehouse, department, worker, and
        /// time of first activity.
        /// </remarks>
        string Key { get; }

        /// <summary>
        /// The configuration that applies to this activity.
        /// </summary>
        IActivityConfiguration Configuration { get; }

        /// <summary>
        /// The time of first activity, such as the first completed line.
        /// </summary>
        DateTime FirstActivity { get; }

        /// <summary>
        /// The time of latest activity, such as the most recently completed line.
        /// </summary>
        DateTime LatestActivity { get; }

        /// <summary>
        /// The number of lines completed so far.
        /// </summary>
        int CompletedLinesCount { get; }
    }
}
