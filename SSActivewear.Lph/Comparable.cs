﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    public static class Comparable
    {
        public static T Min<T>(T val1, T val2) where T : IComparable<T> => val1.CompareTo(val2) <= 0 ? val1 : val2;

        public static T Max<T>(T val1, T val2) where T : IComparable<T> => val1.CompareTo(val2) >= 0 ? val1 : val2;
    }
}
