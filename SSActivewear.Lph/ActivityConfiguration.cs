﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph
{
    public class ActivityConfiguration : IActivityConfiguration
    {
        public TimeSpan StartOfDayOverhead { get; set; }

        public TimeSpan EndOfDayOverhead { get; set; }

        public bool AssumeStartOnHalfHour { get; set; }

        public TimeSpan StandardBreak { get; set; }

        public TimeSpan StandardLunch { get; set; }

        public bool IsBreakBeforeLunch { get; set; }

        public TimeSpan MinimumTransfer { get; set; }

        public TimeSpan MaximumTurnaround { get; set; }
    }
}
