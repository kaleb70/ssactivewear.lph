﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Moq;
using SSActivewear.Lph.Picking;

namespace SSActivewear.Lph.Test
{
    using static Helpers;

    [TestClass]
    public class PickingActivityEvaluatorTest
    {
        [TestMethod]
        public void TestSingleBatch()
        {
            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0));

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(0, result.InactivePeriods.Length);
            Assert.AreEqual(testShift.Length, result.TotalActiveTime);
        }

        [TestMethod]
        public void TestSingleBatchWithInactivePeriod()
        {
            var inactiveLength = TmSpan(0, 20);

            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0))
                            .ContinueBatch(5, inactiveLength);

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(1, result.InactivePeriods.Length);
            var inactive = result.InactivePeriods[0];
            Assert.AreEqual(inactiveLength, inactive.Length);
            Assert.IsFalse(inactive.IsPotentialTransfer);

            Assert.AreEqual(testShift.Length - inactive.Length, result.TotalActiveTime);
        }

        [TestMethod]
        public void TestDefaultMinimumInactivePeriodBoundary()
        {
            var inactiveLength = TmSpan(0, 15);
            var nonBreakingDelayLength = TmSpan(0, 14);

            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0))
                            .ContinueBatch(5, inactiveLength)
                            .ContinueBatch(5, nonBreakingDelayLength);

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(1, result.InactivePeriods.Length);
            var inactive = result.InactivePeriods[0];
            Assert.AreEqual(inactiveLength, inactive.Length);
            Assert.IsFalse(inactive.IsPotentialTransfer);

            Assert.AreEqual(testShift.Length - inactive.Length, result.TotalActiveTime);
        }

        [TestMethod]
        public void TestMinimumInactivePeriodBoundaryFromStandardBreak()
        {
            var config = CreatePickingTestConfiguration();
            config.StandardBreak = TmSpan(0, 18);
            config.MinimumTransfer = TmSpan(0, 19);

            var inactiveLength = TmSpan(0, 18);
            var nonBreakingDelayLength = TmSpan(0, 17);

            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0))
                            .ContinueBatch(5, inactiveLength)
                            .ContinueBatch(5, nonBreakingDelayLength);

            var activityProvider = StandardMockProvider(testShift, config);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(1, result.InactivePeriods.Length);
            var inactive = result.InactivePeriods[0];
            Assert.AreEqual(inactiveLength, inactive.Length);
            Assert.IsFalse(inactive.IsPotentialTransfer);

            Assert.AreEqual(testShift.Length - inactive.Length, result.TotalActiveTime);
        }

        [TestMethod]
        public void TestMinimumInactivePeriodBoundaryFromMinTransfer()
        {
            var config = CreatePickingTestConfiguration();
            config.MinimumTransfer = TmSpan(0, 19);
            config.StandardBreak = TmSpan(0, 17);

            var inactiveLength = TmSpan(0, 17);
            var nonBreakingDelayLength = TmSpan(0, 16);

            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0))
                            .ContinueBatch(5, inactiveLength)
                            .ContinueBatch(5, nonBreakingDelayLength);

            var activityProvider = StandardMockProvider(testShift, config);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(1, result.InactivePeriods.Length);
            var inactive = result.InactivePeriods[0];
            Assert.AreEqual(inactiveLength, inactive.Length);
            Assert.IsFalse(inactive.IsPotentialTransfer);

            Assert.AreEqual(testShift.Length - inactive.Length, result.TotalActiveTime);
        }

        [TestMethod]
        public void TestSingleBatchWithTwoInactivePeriods()
        {
            var inactiveLengths = new[]
            {
                TmSpan(0, 20),
                TmSpan(0, 15),
            };

            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0))
                            .ContinueBatch(5, inactiveLengths[0])
                            .ContinueBatch(5, inactiveLengths[1]);

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(2, result.InactivePeriods.Length);
            CollectionAssert.AreEqual(inactiveLengths, result.InactivePeriods.Select(ip => ip.Length).ToArray());
            Assert.IsFalse(result.InactivePeriods.Any(ip => ip.IsPotentialTransfer));

            Assert.AreEqual(testShift.Length - inactiveLengths.Sum(), result.TotalActiveTime);
        }

        [TestMethod]
        public void TestTwoBatches()
        {
            var turnaround = TmSpan(0, 8);

            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0))
                            .NextBatch(5, turnaround);

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(0, result.InactivePeriods.Length);
            Assert.AreEqual(testShift.Length, result.TotalActiveTime);
            Assert.AreEqual(turnaround, result.AverageTurnaround);
        }

        [TestMethod]
        public void TestThreeBatches()
        {
            var turnarounds = new[]
            {
                TmSpan(0, 8),
                TmSpan(0, 4),
            };

            var testShift = new TestShift()
                            .FirstBatch(5, DtTime(8, 0))
                            .NextBatch(5, turnarounds[0])
                            .NextBatch(5, turnarounds[1]);

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(0, result.InactivePeriods.Length);
            Assert.AreEqual(testShift.Length, result.TotalActiveTime);
            Assert.AreEqual(turnarounds.Average(), result.AverageTurnaround);
        }

        [TestMethod]
        public void TestManyPausesInActivity()
        {
            var pause = new[]
            {
                new { Length = TmSpan(0, 4), NewBatch = false }, // distracted
                new { Length = TmSpan(0, 8), NewBatch = true }, // new batch
                new { Length = TmSpan(0, 11), NewBatch = true }, // new batch
                new { Length = TmSpan(0, 5), NewBatch = true }, // new batch
                new { Length = TmSpan(0, 35), NewBatch = false }, // lunch
                new { Length = TmSpan(0, 4), NewBatch = true }, // new batch
                new { Length = TmSpan(0, 7), NewBatch = false }, // distracted
                new { Length = TmSpan(0, 6), NewBatch = true }, // new batch
                new { Length = TmSpan(0, 22), NewBatch = false }, // break
                new { Length = TmSpan(0, 6), NewBatch = true }, // new batch
            };

            var testShift = new TestShift();
            testShift.FirstBatch(5, DtTime(8, 0));
            foreach (var p in pause)
            {
                if (p.NewBatch)
                    testShift.NextBatch(5, p.Length);
                else
                    testShift.ContinueBatch(5, p.Length);
            }

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            var inactivePeriods = pause.Where(p => p.Length.TotalMinutes >= 15);
            Assert.AreEqual(inactivePeriods.Count(), result.InactivePeriods.Length);
            CollectionAssert.AreEqual(inactivePeriods.Select(ip => ip.Length).ToArray(), result.InactivePeriods.Select(ip => ip.Length).ToArray());
            CollectionAssert.AreEqual(inactivePeriods.Select(ip => ip.NewBatch).ToArray(), result.InactivePeriods.Select(ip => ip.IsPotentialTransfer).ToArray());

            Assert.AreEqual(testShift.Length - inactivePeriods.Sum(ip => ip.Length), result.TotalActiveTime);
            Assert.AreEqual(pause.Where(g => g.NewBatch && g.Length.TotalMinutes <= 10).Average(g => g.Length), result.AverageTurnaround);
        }

        [TestMethod]
        public void TestDefaultMaxTurnaround()
        {
            var turnarounds = new[]
            {
                TmSpan(0, 1),
                TmSpan(0, 2),
                TmSpan(0, 10),
                TmSpan(0, 11),
            };

            var testShift = new TestShift();
            testShift.FirstBatch(5, DtTime(8, 0));
            foreach (var t in turnarounds)
                testShift.NextBatch(5, t);

            var activityProvider = StandardMockProvider(testShift);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(0, result.InactivePeriods.Length);
            Assert.AreEqual(testShift.Length, result.TotalActiveTime);
            Assert.AreEqual(turnarounds.Where(t => t.TotalMinutes <= 10).Average(), result.AverageTurnaround);
        }

        [TestMethod]
        public void TestAlternateMaxTurnaround()
        {
            var config = CreatePickingTestConfiguration();
            config.MaximumTurnaround = TmSpan(0, 9);

            var turnarounds = new[]
            {
                TmSpan(0, 1),
                TmSpan(0, 2),
                TmSpan(0, 9),
                TmSpan(0, 10),
            };

            var testShift = new TestShift();
            testShift.FirstBatch(5, DtTime(8, 0));
            foreach (var t in turnarounds)
                testShift.NextBatch(5, t);

            var activityProvider = StandardMockProvider(testShift, config);

            var result = new PickingActivityEvaluator().EvaluateActivity(activityProvider.Object);

            Assert.AreEqual(0, result.InactivePeriods.Length);
            Assert.AreEqual(testShift.Length, result.TotalActiveTime);
            Assert.AreEqual(turnarounds.Where(t => t.TotalMinutes <= 9).Average(), result.AverageTurnaround);
        }

        static Mock<IPickingActivityProvider> StandardMockProvider(TestShift testShift) =>
            StandardMockProvider(testShift, CreatePickingTestConfiguration());

        static Mock<IPickingActivityProvider> StandardMockProvider(TestShift testShift, ActivityConfiguration configuration)
        {
            var activityProvider = new Mock<IPickingActivityProvider>(MockBehavior.Strict);
            activityProvider.SetupGet(ap => ap.Configuration).Returns(configuration);
            activityProvider.SetupGet(ap => ap.FirstActivity).Returns(testShift.FirstActivity);
            activityProvider.SetupGet(ap => ap.FirstActivity).Returns(testShift.LatestActivity);
            activityProvider.SetupGet(ap => ap.CompletedLinesCount).Returns(testShift.Lines.Count);
            activityProvider.Setup(ap => ap.GetCompletedLines()).Returns(testShift.Lines.ToArray());
            return activityProvider;
        }

        public class TestShift
        {
            private readonly Random random = new Random();

            public DateTime FirstActivity { get; private set; }

            public DateTime LatestActivity { get; private set; }

            public TimeSpan Length => LatestActivity - FirstActivity;

            private int currentBatchID = 0;

            public List<PickingLine> Lines { get; } = new List<PickingLine>();

            public TestShift FirstBatch(int lineCount, DateTime firstActivity)
            {
                FirstActivity = LatestActivity = firstActivity;
                return NextBatch(lineCount, new TimeSpan(0));
            }

            public TestShift NextBatch(int lineCount, TimeSpan turnaround)
            {
                var lph = random.Next(45, 75);
                var pickLength = new TimeSpan(0, 0, 3600 / lph);

                ++currentBatchID;
                LatestActivity += turnaround - pickLength;
                while (lineCount-- > 0)
                {
                    LatestActivity += pickLength;
                    Lines.Add(new PickingLine(currentBatchID, LatestActivity));
                }
                return this;
            }

            public TestShift ContinueBatch(int lineCount, TimeSpan pause)
            {
                --currentBatchID;
                return NextBatch(lineCount, pause);
            }
        }
    }
}
