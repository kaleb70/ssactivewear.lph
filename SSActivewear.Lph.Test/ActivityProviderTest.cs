﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSActivewear.Lph.Test
{
    using static Helpers;

    [TestClass]
    public class ActivityProviderTest
    {
        private readonly Random random = new Random();

        [TestMethod]
        public void TestKey()
        {
            var latestActivity = DateTime.Now;
            var firstActivity = latestActivity.AddMinutes(random.Next(-120, -30));
            var completedLinesCount = random.Next(20, 60);

            var provider = new ConcreteActivityProvider("", new ActivityConfiguration(), firstActivity, latestActivity, completedLinesCount);
            var key = provider.Key;

            Assert.IsTrue(key.Contains(firstActivity.ToString()));
            Assert.IsFalse(key.Contains(latestActivity.ToString()));
            Assert.AreSame(key, provider.Key);
        }

        [TestMethod]
        public void TestConfiguration()
        {
            var config = new ActivityConfiguration();
            var provider = new ConcreteActivityProvider("", config, default(DateTime), default(DateTime), 0);
            Assert.AreSame(config, provider.Configuration);
        }

        [TestMethod]
        public void TestPreloadedValues()
        {
            var latestActivity = DateTime.Now;
            var firstActivity = latestActivity.AddMinutes(random.Next(-120, -30));
            var completedLinesCount = random.Next(20, 60);

            var provider = new ConcreteActivityProvider("SubclassKey", new ActivityConfiguration(), firstActivity, latestActivity, completedLinesCount);

            Assert.IsTrue(provider.Key.Contains("SubclassKey"));
            Assert.AreEqual(firstActivity, provider.FirstActivity);
            Assert.AreEqual(latestActivity, provider.LatestActivity);
            Assert.AreEqual(completedLinesCount, provider.CompletedLinesCount);
        }
    }
}
