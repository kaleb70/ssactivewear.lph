﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SSActivewear.Lph.Picking;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;

namespace SSActivewear.Lph.Test
{
    [TestClass]
    public class PickingActivityProviderTest
    {
        private readonly Random random = new Random();

        [TestMethod]
        public void TestDefaultConfiguration()
        {
            var getCompletedLines = new Mock<IMockFunc<PickingLine[]>>(MockBehavior.Strict);

            var provider = new PickingActivityProvider("", default(DateTime), default(DateTime), 0, getCompletedLines.Object.Invoke);
            var config = provider.Configuration;

            Assert.AreEqual(new TimeSpan(0, 10, 0), config.StartOfDayOverhead);
            Assert.AreEqual(new TimeSpan(0, 10, 0), config.EndOfDayOverhead);

            Assert.IsTrue(config.AssumeStartOnHalfHour);

            Assert.AreEqual(new TimeSpan(0, 15, 0), config.StandardBreak);
            Assert.AreEqual(new TimeSpan(0, 30, 0), config.StandardLunch);
            Assert.IsFalse(config.IsBreakBeforeLunch);

            Assert.AreEqual(new TimeSpan(0, 20, 0), config.MinimumTransfer);
            Assert.AreEqual(new TimeSpan(0, 10, 0), config.MaximumTurnaround);
        }

        [TestMethod]
        public void TestPreloadedValues()
        {
            var latestActivity = DateTime.Now;
            var firstActivity = latestActivity.AddMinutes(random.Next(-120, -30));
            int completedLinesCount = random.Next(1, 100);

            var getCompletedLines = new Mock<IMockFunc<PickingLine[]>>(MockBehavior.Strict);

            var provider = new PickingActivityProvider("PickingActivityProviderTest", firstActivity, latestActivity, completedLinesCount, getCompletedLines.Object.Invoke);

            Assert.IsTrue(provider.Key.Contains("PickingActivityProviderTest"));
            Assert.AreEqual(firstActivity, provider.FirstActivity);
            Assert.AreEqual(latestActivity, provider.LatestActivity);
            Assert.AreEqual(completedLinesCount, provider.CompletedLinesCount);
        }

        [TestMethod]
        public void TestGetCompletedLines()
        {
            var completedLines = new PickingLine[0];

            var getCompletedLines = new Mock<IMockFunc<PickingLine[]>>(MockBehavior.Loose);
            getCompletedLines.Setup(gpl => gpl.Invoke()).Returns(completedLines);

            var provider = new PickingActivityProvider("", default(DateTime), default(DateTime), 0, getCompletedLines.Object.Invoke);

            Assert.AreSame(completedLines, provider.GetCompletedLines());

            getCompletedLines.Verify(gpl => gpl.Invoke(), Times.Once);
        }
    }
}
