﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Moq;

namespace SSActivewear.Lph.Test
{
    using static Helpers;

    [TestClass]
    public class InactivityEvaluatorTest
    {
        [TestMethod]
        public void TestNoInactivity()
        {
            var activityProvider = new ConcreteActivityProvider("", CreatePickingTestConfiguration(), DtTime(8, 5), DtTime(18, 20), 500);
            var activityResult = new TestActivityResult(activityProvider, null);

            var countedInactiveTime = new InactivityEvaluator().EvaluateInactivity(activityProvider, activityResult);
            Assert.AreEqual(new TimeSpan(0), countedInactiveTime);
        }

        [TestMethod]
        public void TestOrdinaryLunchBreak()
        {
            var lunchLength = TmSpan(0, 38);

            var activityProvider = new ConcreteActivityProvider("", CreatePickingTestConfiguration(), DtTime(8, 5), DtTime(18, 20), 500);
            var activityResult = new TestActivityResult(activityProvider, null)
                                    .AddInactivePeriod(DtTime(12, 27), lunchLength, false);

            var countedInactiveTime = new InactivityEvaluator().EvaluateInactivity(activityProvider, activityResult);
            Assert.AreEqual(lunchLength.AddMinutes(-30), countedInactiveTime);
        }

        [TestMethod]
        public void TestLunchBreakSkippedMorningBreak()
        {
            var lunchLength = TmSpan(0, 34);

            ActivityConfiguration config = CreatePickingTestConfiguration();
            config.IsBreakBeforeLunch = true;

            var activityProvider = new ConcreteActivityProvider("", config, DtTime(8, 5), DtTime(18, 20), 500);
            var activityResult = new TestActivityResult(activityProvider, null)
                                    .AddInactivePeriod(DtTime(12, 27), lunchLength, false);

            var countedInactiveTime = new InactivityEvaluator().EvaluateInactivity(activityProvider, activityResult);
            Assert.AreEqual(lunchLength.AddMinutes(-30), countedInactiveTime);
        }

        [TestMethod]
        public void TestLongLunchBreak()
        {
            var lunchLength = TmSpan(1, 15);

            ActivityConfiguration config = CreatePickingTestConfiguration();

            var activityProvider = new ConcreteActivityProvider("", config, DtTime(8, 5), DtTime(18, 20), 500);
            var activityResult = new TestActivityResult(activityProvider, null)
                                    .AddInactivePeriod(DtTime(12, 27), lunchLength, false);

            var countedInactiveTime = new InactivityEvaluator().EvaluateInactivity(activityProvider, activityResult);
            Assert.AreEqual(config.StartOfDayOverhead + config.EndOfDayOverhead, countedInactiveTime);
        }

        [TestMethod]
        public void TestLunchAndBreaks()
        {
            var morningBreakLength = TmSpan(0, 18);
            var lunchLength = TmSpan(0, 34);
            var afternoonBreakLength = TmSpan(0, 23);

            ActivityConfiguration config = CreatePickingTestConfiguration();

            var activityProvider = new ConcreteActivityProvider("", config, DtTime(8, 5), DtTime(18, 20), 500);
            var activityResult = new TestActivityResult(activityProvider, null)
                                    .AddInactivePeriod(DtTime(10, 47), morningBreakLength, false)
                                    .AddInactivePeriod(DtTime(12, 27), lunchLength, false)
                                    .AddInactivePeriod(DtTime(14, 57), afternoonBreakLength, false);

            var countedInactiveTime = new InactivityEvaluator().EvaluateInactivity(activityProvider, activityResult);

            Assert.AreEqual(morningBreakLength + lunchLength.AddMinutes(-30) + afternoonBreakLength,
                            countedInactiveTime);
        }

        [TestMethod]
        public void TestMorningBreakAfternoonTransfer()
        {
            var morningBreakLength = TmSpan(0, 23);
            var lunchLength = TmSpan(0, 34);
            var afternoonBreakLength = TmSpan(0, 28);

            ActivityConfiguration config = CreatePickingTestConfiguration();
            config.IsBreakBeforeLunch = true;

            var activityProvider = new ConcreteActivityProvider("", config, DtTime(8, 5), DtTime(18, 20), 500);
            var activityResult = new TestActivityResult(activityProvider, null)
                                    .AddInactivePeriod(DtTime(10, 47), morningBreakLength, true)
                                    .AddInactivePeriod(DtTime(12, 27), lunchLength, false)
                                    .AddInactivePeriod(DtTime(14, 57), afternoonBreakLength, true);

            var countedInactiveTime = new InactivityEvaluator().EvaluateInactivity(activityProvider, activityResult);

            Assert.AreEqual(morningBreakLength + lunchLength.AddMinutes(-30) + config.MaximumTurnaround,
                            countedInactiveTime);
        }

        [TestMethod]
        public void TestMorningTransferAfternoonBreak()
        {
            var morningBreakLength = TmSpan(0, 23);
            var lunchLength = TmSpan(0, 34);
            var afternoonBreakLength = TmSpan(0, 28);

            ActivityConfiguration config = CreatePickingTestConfiguration();

            var activityProvider = new ConcreteActivityProvider("", config, DtTime(8, 5), DtTime(18, 20), 500);
            var activityResult = new TestActivityResult(activityProvider, null)
                                    .AddInactivePeriod(DtTime(10, 47), morningBreakLength, true)
                                    .AddInactivePeriod(DtTime(12, 27), lunchLength, false)
                                    .AddInactivePeriod(DtTime(14, 57), afternoonBreakLength, true);

            var countedInactiveTime = new InactivityEvaluator().EvaluateInactivity(activityProvider, activityResult);

            Assert.AreEqual(config.MaximumTurnaround + lunchLength.AddMinutes(-30) + afternoonBreakLength,
                            countedInactiveTime);
        }

        [TestMethod]
        public void TestSimpleStartAndEndOverhead()
        {
            var evaluator = new InactivityEvaluator();
            int startOfDayMinutes = 9;
            int endOfDayMinutes = 12;

            Action<TimeSpan, int> runTest = (expected, firstMinute) =>
            {
                var mockProvider = new Mock<IActivityProvider>(MockBehavior.Strict);
                mockProvider.SetupGet(p => p.Configuration).Returns(new ActivityConfiguration
                {
                    AssumeStartOnHalfHour = false,
                    StartOfDayOverhead = TmSpan(0, startOfDayMinutes),
                    EndOfDayOverhead = TmSpan(0, endOfDayMinutes),
                });
                mockProvider.SetupGet(p => p.FirstActivity).Returns(DtTime(9, firstMinute));
                Assert.AreEqual(expected, evaluator.GetOverhead(mockProvider.Object));
            };

            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 0);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 4);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 5);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 10);

            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 30);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 34);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 35);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 40);
        }

        [TestMethod]
        public void TestStartAndEndOverheadOnHalfHour()
        {
            var evaluator = new InactivityEvaluator();
            int startOfDayMinutes = 9;
            int endOfDayMinutes = 12;

            Action<TimeSpan, int> runTest = (expected, firstMinute) =>
            {
                var mockProvider = new Mock<IActivityProvider>(MockBehavior.Strict);
                mockProvider.SetupGet(p => p.Configuration).Returns(new ActivityConfiguration
                {
                    AssumeStartOnHalfHour = true,
                    StartOfDayOverhead = TmSpan(0, startOfDayMinutes),
                    EndOfDayOverhead = TmSpan(0, endOfDayMinutes),
                });
                mockProvider.SetupGet(p => p.FirstActivity).Returns(DtTime(9, firstMinute));
                Assert.AreEqual(expected, evaluator.GetOverhead(mockProvider.Object));
            };

            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 0);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 4);
            runTest(TmSpan(0, 10 + endOfDayMinutes), 5);
            runTest(TmSpan(0, 15 + endOfDayMinutes), 10);

            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 30);
            runTest(TmSpan(0, startOfDayMinutes + endOfDayMinutes), 34);
            runTest(TmSpan(0, 10 + endOfDayMinutes), 35);
            runTest(TmSpan(0, 15 + endOfDayMinutes), 40);
        }

        public class TestActivityResult : ActivityResult
        {
            public TestActivityResult(IActivityProvider activityProvider, TimeSpan? averageTurnaround)
            {
                TotalActiveTime = activityProvider.LatestActivity - activityProvider.FirstActivity;
                AverageTurnaround = averageTurnaround;
                InactivePeriods = new InactivePeriod[0];
            }

            public TestActivityResult AddInactivePeriod(DateTime startTime, TimeSpan length, bool isPotentialTransfer)
            {
                InactivePeriods = InactivePeriods.Append(new InactivePeriod()
                {
                    StartTime = startTime,
                    EndTime = startTime + length,
                    IsPotentialTransfer = isPotentialTransfer,
                }).ToArray();
                TotalActiveTime -= length;
                return this;
            }
        }
    }
}
