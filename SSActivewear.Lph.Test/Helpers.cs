﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSActivewear.Lph.Test
{
    public static class Helpers
    {
        /// <summary>
        /// A convenience method for brevity.
        /// </summary>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static TimeSpan TmSpan(int hour, int minute) => new TimeSpan(hour, minute, 0);

        /// <summary>
        /// A convenience method for brevity.
        /// </summary>
        /// <param name="hour"></param>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static DateTime DtTime(int hour, int minute) => new DateTime(2021, 7, 1, hour, minute, 0);

        public static TimeSpan Sum(this IEnumerable<TimeSpan> collection)
                => new TimeSpan(collection.Sum(ts => ts.Ticks));

        public static TimeSpan Sum<T>(this IEnumerable<T> collection, Func<T, TimeSpan> selector)
                => new TimeSpan(collection.Sum(e => selector(e).Ticks));

        public static TimeSpan Average(this IEnumerable<TimeSpan> collection)
                => new TimeSpan((long)collection.Average(t => t.Ticks));

        public static TimeSpan Average<T>(this IEnumerable<T> collection, Func<T, TimeSpan> selector)
                => new TimeSpan((long)collection.Average(e => selector(e).Ticks));

        public static ActivityConfiguration CreatePickingTestConfiguration() => new ActivityConfiguration()
        {
            StartOfDayOverhead = new TimeSpan(0, 10, 0),
            EndOfDayOverhead = new TimeSpan(0, 10, 0),
            AssumeStartOnHalfHour = true,
            StandardBreak = new TimeSpan(0, 15, 0),
            StandardLunch = new TimeSpan(0, 30, 0),
            IsBreakBeforeLunch = false,
            MinimumTransfer = new TimeSpan(0, 20, 0),
            MaximumTurnaround = new TimeSpan(0, 10, 0)
        };

        public class ConcreteActivityProvider : ActivityProvider
        {
            public ConcreteActivityProvider(string key, IActivityConfiguration configuration, DateTime firstActivity, DateTime latestActivity, int completedLinesCount)
                : base(key, configuration, firstActivity, latestActivity, completedLinesCount)
            {
            }
        }
    }
}
