﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using System.Runtime.Caching;

namespace SSActivewear.Lph.Test
{
    using static Helpers;
    using CacheEntry = LphTimeEstimator.CacheEntry;

    [TestClass]
    public class LphTimeEstimatorTest
    {
        [TestMethod]
        public void TestExistingCacheEntry()
        {
            var activityProvider = new ConcreteActivityProvider("", CreatePickingTestConfiguration(), DtTime(8, 0), DtTime(10, 0), 100);
            var context = new TestContext(activityProvider, TmSpan(0, 10));

            context.SetupCacheGet();

            var timeEstimator = new LphTimeEstimator<IActivityProvider, IActivityEvaluator<IActivityProvider>>(
                                    context.activityEvaluator.Object, context.inactivityEvaluator.Object, context.cache.Object);

            Assert.AreEqual(activityProvider.LatestActivity - activityProvider.FirstActivity, timeEstimator.GetLphTime(activityProvider));

            context.VerifyAll();
        }

        [TestMethod]
        public void TestNoCacheEntry()
        {
            var activityProvider = new ConcreteActivityProvider("", CreatePickingTestConfiguration(), DtTime(8, 0), DtTime(10, 0), 100);
            var context = new TestContext(activityProvider);

            context.SetupCacheGet();
            context.SetupFullCalculation();
            context.SetupCacheSet();

            var timeEstimator = new LphTimeEstimator<IActivityProvider, IActivityEvaluator<IActivityProvider>>(
                                    context.activityEvaluator.Object, context.inactivityEvaluator.Object, context.cache.Object);

            Assert.AreEqual(activityProvider.LatestActivity
                                - activityProvider.FirstActivity
                                + activityProvider.Configuration.StartOfDayOverhead
                                + activityProvider.Configuration.EndOfDayOverhead,
                            timeEstimator.GetLphTime(activityProvider));

            context.VerifyAll();
        }

        [TestMethod]
        public void TestObsoleteCacheEntry()
        {
            var activityProvider = new ConcreteActivityProvider("", CreatePickingTestConfiguration(), DtTime(8, 0), DtTime(10, 0), 100);
            var context = new TestContext(activityProvider, activityProvider.Configuration.MinimumTransfer.AddMinutes(1));

            context.SetupCacheGet();
            context.SetupFullCalculation();
            context.SetupCacheSet();

            var timeEstimator = new LphTimeEstimator<IActivityProvider, IActivityEvaluator<IActivityProvider>>(
                                    context.activityEvaluator.Object, context.inactivityEvaluator.Object, context.cache.Object);

            Assert.AreEqual(activityProvider.LatestActivity
                                - activityProvider.FirstActivity
                                + activityProvider.Configuration.StartOfDayOverhead
                                + activityProvider.Configuration.EndOfDayOverhead,
                            timeEstimator.GetLphTime(activityProvider));

            context.VerifyAll();
        }

        [TestMethod]
        public void TestOutOfSequenceCacheEntry()
        {
            var activityProvider = new ConcreteActivityProvider("", CreatePickingTestConfiguration(), DtTime(8, 0), DtTime(10, 0), 100);
            var context = new TestContext(activityProvider, TmSpan(0, -1));

            context.SetupCacheGet();
            context.SetupFullCalculation();
            context.SetupCacheSet();

            var timeEstimator = new LphTimeEstimator<IActivityProvider, IActivityEvaluator<IActivityProvider>>(
                                    context.activityEvaluator.Object, context.inactivityEvaluator.Object, context.cache.Object);

            Assert.AreEqual(activityProvider.LatestActivity
                                - activityProvider.FirstActivity
                                + activityProvider.Configuration.StartOfDayOverhead
                                + activityProvider.Configuration.EndOfDayOverhead,
                            timeEstimator.GetLphTime(activityProvider));

            context.VerifyAll();
        }

        internal class TestContext
        {
            public readonly CacheEntry existingCacheEntry;
            public readonly ActivityResult activityResult;

            public readonly IActivityProvider activityProvider;
            public readonly string key;

            public bool CacheHasBeenSet { get; private set; } = false;

            public readonly Mock<IActivityEvaluator<IActivityProvider>> activityEvaluator = new Mock<IActivityEvaluator<IActivityProvider>>(MockBehavior.Strict);
            public readonly Mock<IInactivityEvaluator> inactivityEvaluator = new Mock<IInactivityEvaluator>(MockBehavior.Strict);
            public readonly Mock<ObjectCache> cache = new Mock<ObjectCache>(MockBehavior.Strict);

            public TestContext(IActivityProvider activityProvider, TimeSpan cacheEntryAge)
                : this(activityProvider)
            {
                var cacheLatestActivity = activityProvider.LatestActivity - cacheEntryAge;
                existingCacheEntry = new CacheEntry { LatestActivity = cacheLatestActivity, LphTime = cacheLatestActivity - activityProvider.FirstActivity };
            }

            public TestContext(IActivityProvider activityProvider)
            {
                this.activityProvider = activityProvider;
                existingCacheEntry = null;
                activityResult = new ActivityResult
                {
                    TotalActiveTime = activityProvider.LatestActivity
                                        - activityProvider.FirstActivity
                                        - activityProvider.Configuration.StandardBreak
                };
                key = "SSActivewear.Lph;" + activityProvider.Key;
            }

            public void SetupCacheGet()
            {
                cache.Setup(c => c.Get(key, null))
                        .Callback<string, string>((k, r) => Assert.IsFalse(CacheHasBeenSet))
                        .Returns(existingCacheEntry);
            }

            public void SetupCacheSet()
            {
                var expectedCacheEntry = new CacheEntry
                {
                    LatestActivity = activityProvider.LatestActivity,
                    LphTime = activityProvider.LatestActivity
                                - activityProvider.FirstActivity
                                + activityProvider.Configuration.StartOfDayOverhead
                                + activityProvider.Configuration.EndOfDayOverhead,
                };

                cache.Setup(c => c.Set(key, It.IsAny<object>(), It.IsAny<CacheItemPolicy>(), null))
                        .Callback<string, object, CacheItemPolicy, string>((k, v, p, r) =>
                        {
                            Assert.IsFalse(CacheHasBeenSet);
                            CacheHasBeenSet = true;
                            Assert.AreNotSame(existingCacheEntry, v);
                            Assert.AreEqual(expectedCacheEntry, v);
                            Assert.IsTrue((DateTimeOffset.Now + activityProvider.Configuration.MinimumTransfer - p.AbsoluteExpiration).TotalMinutes < 1);
                        });
            }

            public void SetupFullCalculation()
            {
                inactivityEvaluator.Setup(ie => ie.GetOverhead(activityProvider))
                                    .Returns(activityProvider.Configuration.StartOfDayOverhead + activityProvider.Configuration.EndOfDayOverhead);

                activityEvaluator.Setup(ae => ae.EvaluateActivity(activityProvider)).Returns(activityResult);

                inactivityEvaluator.Setup(ie => ie.EvaluateInactivity(activityProvider, activityResult))
                                    .Returns(activityProvider.Configuration.StandardBreak);
            }

            public void VerifyAll()
            {
                activityEvaluator.VerifyAll();
                inactivityEvaluator.VerifyAll();
                cache.VerifyAll();
            }
        }
    }
}
